var gulp = require('gulp');
var sassdoc = require('sassdoc');

gulp.task('sassdoc', function () {
	var options = {
		dest: 'docs',
		verbose: true,
		autofill: ["requires", "throws", "content"],
		groups: {
			'undefined': 'Ungrouped'
		}
	};

	return gulp.src('./*.scss')
		.pipe(sassdoc(options));
});