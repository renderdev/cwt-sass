////
/// @group Text
////

///
/// Resets the font
///
/// @example scss
///   @include reset-box-model;
///
@mixin reset-font {
	font:           inherit;
	font-size:      100%;
	vertical-align: baseline;
}

///
/// Resets the line height
///
/// @example scss
///   @include reset-body;
///
@mixin reset-body {
	line-height: 1;
}

///
/// @alias reset-body
///
@mixin reset-line-height {
	@include reset-body;
}

///
/// Resets font and line-height
/// @see {mixin} reset-font
/// @see {mixin} reset-body
///
/// @require {mixin} reset-font
/// @require {mixin} reset-body
///
/// @example scss
///   @include cwt-clear-font;
///
@mixin clear-font {
	@include reset-font;
	@include reset-body;
}

///
/// Hide overflow text and sets overflow to ellipsis
///
/// @example scss
///   @include text-overflow;
///
@mixin text-overflow {
	overflow:      hidden;
	text-overflow: ellipsis;
	white-space:   nowrap;
}

///
/// Truncates text and adds an ellipsis to represent overflow.
/// @link http://bourbon.io/docs/#ellipsis Bourbon: Ellipsis
///
/// @param {Number} $width [100%] - The `max-width` for the string to respect before being truncated.
/// @param {String} $display [inline-block] - Sets the display-value of the element.
///
/// @require {mixin} text-overflow
///
/// @example scss
///     @include ellipsis(50%);
///
@mixin ellipsis($max-width: 100%) {
	display:   inline-block;
	max-width: $max-width;
	word-wrap: normal;
	@include text-overflow;
}

///
/// CSS image replacement technique
///
/// @link https://github.com/h5bp/html5-boilerplate/commit/aa0396eae757 CSS image replacement
///
/// @example scss
///   @include hide-text;
///
///
@mixin hide-text {
	font:             0/0 a;
	color:            transparent;
	text-shadow:      none;
	background-color: transparent;
	border:           0;
}

///
/// Controls whether the user can select text
/// NOTICE: This mixin is planned to be deprecated in next major release
///
/// @param {user-select} $type - User select type
///
/// @example scss
///   @include user-select(none);
///
@mixin user-select($type: null) {
	user-select: $type;
}

///
/// Copies and converts a property and its value to either from rem > px or px > rem.
/// Both rem and px properties are returned.<br>
/// A global $ignoreRems can be set to true, and the mixin will only return pixels
/// @link https://hugogiraudel.com/2013/03/18/ultimate-rem-mixin/ Hugo Giraudel: The ultimate PX/REM mixin
///
/// @param {String} $property - CSS property
/// @param {Any} $values - CSS value
///
/// @require {function} strip-unit
///
/// @example
///   @include rem(font-size, 1.6rem);
///   @include rem(padding, 20px 10px);
///
@mixin rempx($property, $values) {
	$ignoreRems: false !default;
	$px: ();
	$rem: ();

	@each $value in $values {

		@if $value == 0 or $value == auto {
			$px: append($px, $value);
			$rem: append($rem, $value);
		} @else {
			@if unitless($value) {
				$value: $value * 1px;
			}

			$unit: unit($value);
			$val: strip-unit($value);

			@if $unit == "px" {
				$px: append($px, $value);
				$rem: append($rem, ($val / 10 + rem));
			}

			@if $unit == "rem" {
				$px: append($px, ($val * 10 + px));
				$rem: append($rem, $value);
			}
		}
	}

	@if ($ignoreRems) {
		#{$property}: $values;
	} @else {
		@if $px == $rem {
			#{$property}: $px;
		} @else {
			#{$property}: $px;
			#{$property}: $rem;
		}
	}
}

///
/// Sets the font-family, font-size and line-height of an element
///
/// @param {String} $family - Font Family
/// @param {Length} $size - Font Size
/// @param {Length} $line-height - Line height
///
/// @example scss
///   @include cwt-font(Lato, 16px, 1.4);
///
@mixin cwt-font($family: null, $size: null, $line-height: null) {
	// bump each parameter up one ($family is optional)
	@if type-of($family) == number {
		@if isnotempty($size) {
			$line-height: $size;
		}
		$size: $family;
		$family: null;
	}

	@if $family != null {
		font-family: $family;
	}
	@if $size != null {
		@if unit($size) == "" or unit($size) == "px" or unit($size) == "rem" {
			@include rempx(font-size, $size);
		} @else {
			font-size: $size;
		}
	}
	@if $line-height != null {
		@if $line-height < 6 and unitless($line-height) {
			line-height: $line-height;
		} @else {
			@if unit($line-height) == "" or unit($line-height) == "px" or unit($line-height) == "rem" {
				@include rempx(line-height, $line-height);
			} @else {
				line-height: $line-height;
			}
		}
	}
}

///
/// Converts shorthand options to full css values for Text properties.<br>
/// Accepts: b: bold, c: capitalize, s: caps, i: italics, l: lowercase, w: nowrap, u: underline, p: uppercase
///
/// @param {String} $style - shorthand character
///
/// @return {String} full css value
///
/// @example scss
///   @include expand-style(b);
///
@function expand-style($style) {
	@if $style == b {
		$style: bold;
	} @else if $style == i {
		$style: italics;
	} @else if $style == s {
		$style: caps;
	} @else if $style == u {
		$style: underline;
	} @else if $style == p {
		$style: uppercase;
	} @else if $style == l {
		$style: lowercase;
	} @else if $style == c {
		$style: capitalize;
	} @else if $style == w {
		$style: nowrap;
	}
	@return $style;
}

///
/// Converts multiple shorthand text options to full property and values<br>
/// Accepts: b: bold, c: capitalize, s: caps, i: italics, l: lowercase, w: nowrap, u: underline, p: uppercase
///
/// @param {List | String} $styles - set of shorthand characters
///
/// @require {function} expand-style
///
/// @example scss
///   @include cwt-text(u b);
///
@mixin cwt-text($styles) {
	@each $style in $styles {
		$style: expand-style($style);
		@if $style == bold {
			font-weight: bold;
		}
		@if $style == italics {
			font-style: italic;
		}
		@if $style == caps {
			font-variant: small-caps;
		}
		@if $style == underline {
			text-decoration: underline;
		}
		@if $style == uppercase {
			text-transform: uppercase;
		}
		@if $style == lowercase {
			text-transform: lowercase;
		}
		@if $style == capitalize {
			text-transform: capitalize;
		}
		@if $style == nowrap {
			white-space: nowrap;
		}
	}
}

///
/// Resets multiple shorthand text options back to their default values<br>
/// Accepts: b: bold, c: capitalize, s: caps, i: italics, l: lowercase, w: nowrap, u: underline, p: uppercase
///
/// @param {String} $style - shorthand character
///
/// @require {function} expand-style
///
/// @example scss
///   @include cwt-clear-text(u b)
///
@mixin cwt-clear-text($styles) {
	@each $style in $styles {
		$style: expand-style($style);
		@if $style == bold {
			font-weight: normal;
		}
		@if $style == italics {
			font-style: normal;
		}
		@if $style == caps {
			font-variant: normal;
		}
		@if $style == underline {
			text-decoration: none;
		}
		@if $style == uppercase {
			text-transform: none;
		}
		@if $style == lowercase {
			text-transform: none;
		}
		@if $style == capitalize {
			text-transform: none;
		}
		@if $style == nowrap {
			white-space: normal;
		}
	}
}

///
/// Makes and element/link, and all of its children, have an underline  on hover, focus and active, but not to have an underline in its default state
///
/// @example scss
///   a {
///     @include hover-link;
///   }
///
@mixin hover-link {
	&, & * {
		text-decoration: none;
	}
	&:hover, &:focus, &:active {
		&, * {
			text-decoration: underline;
		}
	}
}

///
/// Removes text-decoration from both the element and its child elements
///
/// @param {String} $cursor [pointer] - Cursor type
///
/// @example scss
///   a {
///     @include no-hover-link(pointer);
///   }
///
@mixin no-hover-link($cursor: pointer) {
	&, &:hover, &:focus, &:active {
		&, * {
			text-decoration: none;
		}
	}
	@if isnotempty(($cursor)) {
		cursor: $cursor;
	}
}

///
/// Sets the colors of the different states of a link<br>
/// If param is true it will match $normal, if it is false it will ignored
///
/// @param {Color} $normal - Normal link color
/// @param {Color | Bool} $hover [$normal] - Hover link color (Optional)
/// @param {Color | Bool} $active [$normal] - Active link color (Optional)
/// @param {Color | Bool} $visited [$normal] - Visited link color (Optional)
/// @param {Color | Bool} $focus [$normal] - Focus link color (Optional)
///
/// @example scss
///   a {
///     @include link-colors(red, dark-rad);
///   }
///
@mixin link-colors($normal, $hover: false, $active: false, $visited: false, $focus: false) {
	color: $normal;

	@if $hover == true {
		$hover: $normal;
	}
	@if $visited == true {
		$visited: $normal;
	}
	@if $active == true {
		$active: $hover;
	}
	@if $focus == true {
		$focus: $hover;
	}

	@if $visited {
		&:visited {
			color: $visited;
		}
	}
	@if $focus {
		&:focus {
			color: $focus;
		}
	}
	@if $hover {
		&:hover {
			color: $hover;
		}
	}
	@if $active {
		&:active {
			color: $active;
		}
	}
}

///
/// Sets the text color, underline decoration and cursor of a link
///
/// @param {Color | Bool} $normal [false] - Normal link color
/// @param {Color | Bool} $hover [true] - Hover link color
/// @param {Color | Bool} $active [true] - Active link color
/// @param {Color | Bool} $visited [true] - Visited link color
/// @param {Color | Bool} $focus [true] - Focus link color
/// @param {Bool} $decorate [true] - If true, adds `text-decoration: underline` on hover and focus.
///
/// @content Applies styles to element
///
/// @require {mixin} link-colors
/// @require {mixin} hover-link
/// @require {mixin} no-hover-link
///
/// @example scss
///   a {
///     @include islink(red, $decorate:false);
///   }
///
@mixin islink($normal: false, $hover: true, $active: true, $visited: true, $focus: true, $decorate: false) {
	@include link-colors($normal, $hover, $active, $visited, $focus);
	@if $decorate {
		@include hover-link;
	} @else {
		@include no-hover-link;
	}
	&:hover {
		cursor: pointer;
	}
	@content;
}

///
/// Sets the text color, underline decoration and cursor of a link. Provides wrapping `a` tag
/// @see {mixin} islink
///
/// @param {Color | Bool} $normal [false] - Normal link color
/// @param {Color | Bool} $hover [true] - Hover link color
/// @param {Color | Bool} $active [true] - Active link color
/// @param {Color | Bool} $visited [true] - Visited link color
/// @param {Color | Bool} $focus [true] - Focus link color
/// @param {Bool} $decorate [true] - If true, adds `text-decoration: underline` on hover and focus.
///
/// @content Applies additional styles to link
///
/// @require {mixin} islink
///
/// @example scss
///   @include link(red, $decorate:false)
///
@mixin link($normal: false, $hover: true, $active: true, $visited: true, $focus: true, $decorate: true) {
	a {
		@include islink($normal, $hover, $active, $visited, $focus, $decorate) {
			@content;
		}
	}
}

///
/// Combines ellipsis and cwt-size
/// @see {mixin} ellipsis
/// @see {mixin} cwt-size
///
/// @param {Length} $width - Element's width
/// @param {Length} $height - Element's height
/// @param {Length} $line-height - Line height
/// @param {String} $box-sizing - Defines box-sizing attribute. `content-box` is default. Use "True" for `border-box`
///
/// @content Applies styles to element
///
/// @require {mixin} ellipsis
/// @require {mixin} cwt-size
///
/// @example scss
///   @include truncate(100px, 200px, 1em, border-box)
///
@mixin truncate($width: null, $height: null, $line-height: null, $box-sizing: null) {
	@include ellipsis;
	@include cwt-size($width, $height, $line-height, $box-sizing);
	@content;
}