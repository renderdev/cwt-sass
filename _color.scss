////
/// @group Color
////

///
/// Checks to see if a list of colors are identical
///
/// @param {List} $colors - Colors to be compared
///
/// @return {Bool}
///
@function is-equal-colors($colors) {
	@for $i from 2 through length($colors) {
		@if ie-hex-str(nth($colors, $i - 1)) != ie-hex-str(nth($colors, $i)) {
			@return false;
		}
	}

	@return true;
}

///
/// Strips alpha value from rgba color
///
/// @param {Color} $color - rgba value
///
/// @return {Color}
///
@function strip-alpha($color) {
	@return rgb(red($color), green($color), blue($color));
}

///
/// Adds dark shading to a color
///
/// @param {Color} $color - Color to be shaded
/// @param {Number} $percent - Percent to be shaded, number between 0 and 1 or equivalent percentage
///
/// @return {Color}
///
/// @example scss
///   @include shade(#30f, 15%);
///
@function shade($color, $percent) {
	@return mix(#000, $color, $percent);
}

///
/// Adds a light tint to a color
///
/// @param {Color} $color - Color to be tinted
/// @param {Number} $percent - Percent to be shaded, number between 0 and 1 or equivalent percentage
///
/// @return {Color}
///
/// @example scss
///   @include tint(#30f, 15%);
///
@function tint($color, $percent) {
	@return mix(#FFF, $color, $percent);
}

///
/// Creates a color with transparency
///
/// @param {Color} $color - Hex color
/// @param {Number} $alpha - Alpha value
///
/// @require {function} isset
///
/// @example scss
///   @include color-rgba(#c0fefe, 0.5);
///
@mixin color-rgba($color, $alpha) {
	@if isset($alpha) and $alpha < 1 {
		color: rgba($color, $alpha);
	} @else {
		color: $color;
	}
}

///
/// Creates a background color with transparency
///
/// @param {Color} $color - Hex color
/// @param {Number} $alpha - Alpha value
///
/// @require {function} isset
///
/// @example scss
///   @include bg-rgba(#c0fefe, 0.5);
///
@mixin bg-rgba($color, $alpha) {
	@if isset($alpha) and $alpha < 1 {
		background-color: rgba($color, $alpha);
	} @else {
		background-color: $color;
	}
}

///
/// Outputs css for text color, bg color and text shadow
///
/// @param {Color} $text - text color
/// @param {Color} $background - bg color
/// @param {Arglist} $text-shadow - text-shadow values (Optional)
///
/// @content Applies styles to element
///
/// @require {function} isnotempty
/// @require {mixin} color-rgba
/// @require {mixin} bg-rgba
///
/// @example scss
///   @include cwt-color(#c0fefe, 0.5);
///
@mixin cwt-color($text: null, $background: null, $text-shadow: null) {
	@if length($background) > 2 {
		$text-shadow: $background;
		$background: null;
	}

	@if isnotempty($text) {
		@if (length($text) == 1) {
			@include color-rgba(strip-alpha($text), alpha($text));
		} @else {
			@include color-rgba(nth($text, 1), nth($text, 2));
		}
	}
	@if isnotempty($background) {
		@if (length($background) == 1) {
			@include bg-rgba(strip-alpha($background), alpha($background));
		} @else {
			@include bg-rgba(nth($background, 1), nth($background, 2));
		}
	}
	@if isnotempty($text-shadow) {
		text-shadow: $text-shadow;
	}
	@content;
}

///
/// Defines css background.<br>
/// If `$color` is `null`, then `transparent` is used.<br>
/// If `$image` is `null`, then this is just a background color<br>
/// If `$image` is `false`, then implies background-image will be an inline style
///
/// @param {Color | Null} $color [transparent] - Background color
/// @param {String | Null} $image - Background Image
/// @param {String} $x [center] - Horizontal Alignment of background  (Optional)
/// @param {String} $y [center] - Vertical Alignment of background  (Optional)
/// @param {String} $repeat [no-repeat] - Background repeat  (Optional)
/// @param {String} $attachment [scroll] - Vertical Alignment of background (Optional)
///
/// @content Applies styles to element
///
/// @require {function} isnotempty
///
/// @example scss
///   @include cwt-bg(transparent, null, center, center, repeat);
///
@mixin cwt-bg($color: transparent, $image: null, $x: center, $y: center, $repeat: no-repeat, $attachment: scroll, $size: null) {
	@if $color == null {
		$color: transparent;
	}
	@if $image == null {
		background: $color;
	} @else if $image == false {
		background: $color $repeat $attachment $x $y;
	} @else {
		background: $color url($image) $repeat $attachment $x $y;
	}
	@if isnotempty($size) {
		@if $image == null {
			background-position: $x $y;
		}
		background-size: $size;
	}
	@content;
}

///
/// Defines background-size and background-position
///
/// @param {String} $size [cover] - Defines background-size
/// @param {String | Null} $position [center center] - Defines background-position (Optional)
///
/// @content Applies styles to element
///
/// @require {function} isnotempty
///
/// @example scss
///   @include cwt-bg-size(cover, top center);
///
@mixin cwt-bg-size($size: cover, $position: center center) {
	@if isnotempty($position) {
		background-position: $position;
	}
	background-size: $size;
	@content;
}

///
/// Sets background-size to cover and background-position to input provided
/// @see {mixin} cwt-bg-cover
///
/// @param {String | Null} $position [center center] - Defines background-position (Optional)
///
/// @content Applies styles to element
///
/// @require {mixin} cwt-bg-cover
///
/// @example scss
///   @include cwt-bg-cover(top center)
///
@mixin cwt-bg-cover($position: center center) {
	@include cwt-bg-size(cover, $position);
	@content;
}

///
/// Defines a background gradient for the body
///
/// @param {Color} $startColor - Background start color
/// @param {Color} $endColor - Background end color
/// @param {Length} $min-height [650px] - Media query min-height
///
/// @require {function} isnotset
///
/// @example scss
///   @include cwt-body-gradient(#f00, #ff0, 300px);
///
@mixin cwt-body-gradient($startColor: null, $endColor: null, $min-height: 650px) {
	@if isnotset($min-height) {
		$min-height: 650px;
	}

	html, body {
		background: linear-gradient($startColor 0px, $endColor $min-height);
	}

	@media (min-height: $min-height) {
		body {
			min-height: $min-height;
		}
	}
}

/// Twitter Brand Color
$twitter-color: #55ACEE;
/// Facebook Brand Color
$facebook-color: #3B5998;
/// Google+ Brand Color
$googleplus-color: #DD4B39;
/// Pinterest Brand Color
$pinterest-color: #CB2027;
/// LinkedIn Brand Color
$linkedin-color: #007BB6;
/// YouTube Brand Color
$youtube-color: #CC181E;
/// Vimeo Brand Color
$vimeo-color: #1AB7EA;
/// Tumblr Brand Color
$tumblr-color: #35465C;
/// Instagram Brand Color
$instagram-color: #3F729B;
/// Flickr Brand Color
$flickr-color: #FF0084;
/// WordPress Brand Color
$wordpress-color: #21759B;
/// Stumbeupon Brand Color
$stumbleupon-color: #EB4924;
/// Yahoo Brand Color
$yahoo-color: #400191;
/// Blogger Brand Color
$blogger-color: #F57D00;