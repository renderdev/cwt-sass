////
/// @group CSS Helpers
////

///
/// Outputs CSS for text that is meant for screen readers
///
/// @output makes classes `.assistive-text` and `.screen-reader-text` available for use
///
/// @require {mixin} absolute
/// @require {mixin} cwt-color
///
/// @example scss
///   @include assistive-text;
///
@mixin assistive-text {
	.assistive-text, .screen-reader-text {
		clip:     rect(1px, 1px, 1px, 1px);
		position: absolute !important;
		&:hover, &:active, &:focus {
			@include cwt-block;
			@include absolute(100000, .5em, null, null, .5em);
			@include cwt-color(black, white);
			clip:    auto !important;
			border:  2px solid #333;
			padding: 1em;
		}
	}
}

///
/// Accepts a wrapping selector and creates helper css classes to be used at provided breakpoints
///
/// @param {String} $orig-selector ["ul"] - CSS selector to apply unset to
/// @param {List} $breakpoints ["ul"] - CSS selector to apply unset to
///
/// @content Applies styles within $orig-selector element at provided breakpoints
///
/// @require {function} isnotempty
/// @require {mixin} at_cwt_bp
///
/// @example scss
///   @include cwt-add-responsive-classes('.left-text') {
///     text-align: left !important;
///   }
///
@mixin cwt-add-responsive-classes($orig-selector: null, $breakpoints: '' 'phone' 'non-phone' 'mobile' 'non-mobile') {
	$selectors: selector-parse($orig-selector);
	$separator: ', ';

	@each $breakpoint in $breakpoints {
		$string: '';
		$new-selector: '';

		@each $selector in $selectors {
			$suffix: if(isnotempty($breakpoint), '-at-'+$breakpoint, $breakpoint);
			$string: str-insert($string, nth($selector, 1)+$suffix+$separator, str-length($string)+1);
		}

		$new-selector: str-slice($string, 0, (str-length($separator)+1)*-1);

		@include at_cwt_bp($breakpoint) {
			#{$new-selector} {
				@content;
			}
		}
	}
}

///
/// Determines if a value is valid given provided include and exclude lists.<br>
/// Order of operations:<br>
///   1) If item is in `$include` then `TRUE`<br>
///   2) If `$include` is `all` AND item is NOT in `$exclude` then `TRUE`<br>
///   3) All other scenarios are `FALSE`
///
/// @param {String} $item [] - Value to check
/// @param {List | String} $include [] - Include list to check against
/// @param {List | String} $exclude [] - Exclude list to check against
///
/// @return {Bool} valid state
///
/// @example scss
///   @if include-item(display, all, float box ratio body) {
///     .show {
///       display: inherit !important;
///     }
///    }
///
@function include-item($item, $include, $exclude) {
	@return index($include, $item) != null or ($include == all and index($exclude, $item) == null);
}

///
/// Outputs helper css classes requested<br>
/// Accepts: assistive display style position float box ratio body & all
/// @see {function} include-item
///
/// @output helper classes
///
/// @param {List | String} $include [default all] - Include List
/// @param {List | String} $exclude [float box ratio body] - Exclude List
///
/// @require {function} include-item
///
/// @example scss
///   @include cwt-css-classes(all, float box ratio body);
///
@mixin cwt-css-classes($include: all, $exclude: float box ratio body) {
	@if include-item(assistive, $include, $exclude) {
		@include assistive-text;
	}

	@if include-item(display, $include, $exclude) {
		.show {
			display: inherit !important;
		}

		.hide {
			display: none !important;
		}

		.hidden {
			visibility: hidden !important;
		}

		.visible {
			visibility: visible !important;
		}

		.hide-text {
			@include hide-text;
		}

		.block {
			display: block !important;
		}

		.inline-block {
			display: inline-block !important;
		}

		.table {
			display: table !important;
		}

		.table-row {
			display: table-row !important;
		}

		.table-cell {
			display: table-cell !important;
		}

		.table-cell-top {
			display:        table-cell !important;
			vertical-align: top !important;
		}

		.table-cell-middle {
			display:        table-cell !important;
			vertical-align: middle !important;
		}

		.table-cell-bottom {
			display:        table-cell !important;
			vertical-align: bottom !important;
		}

		@each $size, $mq in $breakpoint-list {
			.hide-#{$size}, .show-non-#{$size} {
				@include at_cwt_bp($mq) {
					display: none !important;
				}
			}

			.show-#{$size}, .hide-non-#{$size} {
				@include at_cwt_bp($mq, true) {
					display: none !important;
				}
			}
		}

		.hide-print, .show-non-print {
			@include at-print {
				display: none !important;
			}
		}

		.show-print, .hide-non-print {
			@include at-not-print {
				display: none !important;
			}
		}
	}

	@if include-item(style, $include, $exclude) {
		.bold {
			font-weight: bold !important;
		}

		.no-bold {
			font-weight: normal !important;
		}

		.italics {
			font-style: italic !important;
		}

		.no-italics {
			font-style: normal !important;
		}

		.caps {
			font-variant: small-caps !important;
		}

		.no-caps {
			font-variant: normal !important;
		}

		.underline {
			text-decoration: underline !important;
		}

		.no-underline {
			text-decoration: none !important;
		}

		.no-decorate, .no-hover {
			&, &:hover, & a, & a:hover {
				text-decoration: none !important;
			}
		}

		.uppercase {
			text-transform: uppercase !important;
		}

		.lowercase {
			text-transform: lowercase !important;
		}

		.capitalize {
			text-transform: capitalize !important;
		}

		.normal-case {
			text-transform: none !important;
		}

		@include cwt-add-responsive-classes('.nowrap') {
			white-space: nowrap !important;
		}

		@include cwt-add-responsive-classes('.wrap') {
			white-space: normal !important;
		}

		@include cwt-add-responsive-classes('.left-text') {
			text-align: left !important;
		}

		@include cwt-add-responsive-classes('.right-text') {
			text-align: right !important;
		}

		@include cwt-add-responsive-classes('.center-text') {
			text-align: center !important;
		}

		.bg-center-cover {
			@include cwt-bg-cover;
		}
	}

	@if include-item(position, $include, $exclude) {
		.relative {
			position: relative;
		}

		/* Requires relative parent */
		.align-top {
			@include cwt-align-top;
		}

		/* Requires relative parent */
		.align-bottom {
			@include cwt-align-bottom;
		}

		/* Requires relative parent */
		.center-all, .center-block-and-text, .align-center-all {
			@include cwt-center-block;
			text-align: center;
		}

		.center-block, .align-center {
			@include cwt-center-block;
		}

		.center-block-x {
			@include cwt-center-block-x;
		}

		.center-block-y {
			@include cwt-center-block-y;
		}

		.fixed-top {
			@include fixed(null, 0, 0, null, 0);
		}

		.fixed-bottom {
			@include fixed(null, 0, null, 0, 0);
		}
	}

	@if include-item(float, $include, $exclude) {
		@include cwt-add-responsive-classes('.float-wrapper, .float-container') {
			@include cwt-clearfix;
			@include reset-list;
		}

		@include cwt-add-responsive-classes('.float-left') {
			float: left !important;
		}

		@include cwt-add-responsive-classes('.float-left-wrapper, .float-left-container') {
			@include cwt-clearfix;
			@include reset-list;
			> * {
				float: left !important;
			}
		}

		@include cwt-add-responsive-classes('.float-right') {
			float: right !important;
		}

		@include cwt-add-responsive-classes('.float-right-wrapper, .float-right-container') {
			@include cwt-clearfix;
			@include reset-list;
			> * {
				float: right !important;
			}
		}

		@include cwt-add-responsive-classes('.clear-float, .clear-both, .no-float') {
			clear: both !important;
		}
	}

	@if include-item(box, $include, $exclude) {
		.full-width {
			width: 100% !important;
		}

		.full-height {
			height: 100% !important;
		}

		.full-box, .full-block {
			width:  100% !important;
			height: 100% !important;
		}

		.reset-list {
			@include reset-list;
		}

		.reset-box, .reset-box-model {
			@include reset-box-model;
		}

		@each $box in 'margin', 'padding' {
			@each $pos in '', 'top', 'right', 'bottom', 'left' {
				$pos: if(isnotempty($pos), '-'+$pos, $pos);
				@include cwt-add-responsive-classes(".clear-#{$box}#{$pos}, .no-#{$box}#{$pos}") {
					#{$box}#{$pos}: 0 !important;
				}
			}
		}

		.uncollapse-margin {
			@include cwt-uncollapse-margin;
		}

		.collapse-margin {
			@include cwt-collapse-margin;
		}
	}

	@if include-item(ratio, $include, $exclude) {
		@include cwt-add-responsive-classes('.square-ratio') {
			@include cwt-block-ratio; // 1
		}

		@include cwt-add-responsive-classes('.old-monitor-ratio') {
			@include cwt-block-ratio(5, 4); // 1.25
		}

		@include cwt-add-responsive-classes('.standard-ratio, .aps-ratio, .monitor-ratio') {
			@include cwt-block-ratio(4, 3); // 1.33
		}

		@include cwt-add-responsive-classes('.vga-ratio, .dslr-ratio, .apsc-ratio') {
			@include cwt-block-ratio(3, 2); // 1.5
		}

		@include cwt-add-responsive-classes('.golden-ratio') {
			@include cwt-block-ratio(1.618); // 1.618
		}

		@include cwt-add-responsive-classes('.hi-def-ratio, .hd-ratio, .apsh-ratio') {
			@include cwt-block-ratio(16, 9); // 1.778
		}

		@include cwt-add-responsive-classes('.anamorphic-ratio') {
			@include cwt-block-ratio(2.39); // 2.39
		}

		@include cwt-add-responsive-classes('.apsp-ratio') {
			@include cwt-block-ratio(3); // 3
		}
	}

	@if include-item(body, $include, $exclude) {
		// adds => html.cwt-reset { ... }
		@include body-reset($append-to-html-tag: ".cwt-reset");

		// adds => html.cwt-app { ... }
		@include body-reset-app($append-to-html-tag: ".cwt-app");
	}
}

///
/// Outputs CSS for clearing top margin for first children of headers
///
/// @output css
///
/// @example scss
///   @include cwt-clear-top-margin-for-first-child-headers;
///
@mixin cwt-clear-top-margin-for-first-child-headers {
	h1, h2, h3, h4, h5, h6 {
		&:first-child {
			margin-top: 0;
		}
	}
}

///
/// Outputs CSS for making links in headers block level items
///
/// @output css
///
/// @example scss
///   @include cwt-display-block-header-links;
///
@mixin cwt-display-block-header-links {
	h1, h2, h3, h4, h5, h6 {
		> a {
			display: block;
		}
	}
}

///
/// Outputs CSS for making links in lists block level items
///
/// @output css
///
/// @example scss
///   @include cwt-display-block-list-links;
///
@mixin cwt-display-block-list-links {
	li {
		> a {
			display: block;
		}
	}
}

///
/// Outputs CSS for resetting lists for provided selector
///
/// @output css
///
/// @param {String} $selector ["ul"] - CSS selector to apply reset to
///
/// @example scss
///   @include cwt-reset-lists("ul, ol, dl");
///
@mixin cwt-reset-lists($selector: "ul") {
	#{$selector} {
		@include reset-list;
	}
}

///
/// Outputs CSS for making all images responsive
///
/// @output css
///
/// @example scss
///   @include cwt-responsive-images;
///
@mixin cwt-responsive-images {
	img {
		max-width: 100%;
		height:    auto;
	}
}