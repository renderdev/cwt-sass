////
/// @group Box
////

///
/// Resets list style
///
/// @example scss
///   @include reset-list-style;
///
@mixin reset-list-style {
	list-style: none;
}

///
/// Sets the values of margin, padding, and border to `0`
///
/// @example scss
///   @include reset-box-model;
///
@mixin reset-box-model {
	margin:  0;
	padding: 0;
	border:  0;
}

///
/// Combines reset-list-style and reset-box-model
/// @see {mixin} reset-list-style
/// @see {mixin} reset-box-model
///
/// @require {mixin} reset-list-style
/// @require {mixin} reset-box-model
///
/// @example scss
///   @include reset-list;
///
@mixin reset-list {
	@include reset-list-style;
	@include reset-box-model;
}

///
/// Sets list back to default styling
///
/// @param {String} $selector ["ul"] - CSS selector to apply unset to
///
/// @content Applies styles to selector element
///
/// @example scss
///   @include unset-list("ul");
///
@mixin unset-list($selector: "ul") {
	#{$selector} {
		display:         block;
		list-style-type: disc;
		margin-left:     40px;
		@content;
	}
}

///
/// Forces content after the floats or the container containing the floats to render below it
///
/// @example scss
///   @include cwt-clearfix;
///
@mixin cwt-clearfix {
	&::after {
		content: "";
		display: table;
		clear:   both;
	}
}

///
/// @alias cwt-clearfix
///
@mixin float-wrapper {
	@include cwt-clearfix;
}

///
/// Float container
///
/// @param {String} $float - Element's float direction
/// @param {Bool} $reset-list [false] - Resets the element's list styles (Optional)
///
/// @content Applies styles to the children of the floating container element
///
/// @require {mixin} cwt-clearfix
/// @require {mixin} reset-list - used if the float container is a list (ul, ol or dl)
///
/// @example scss
///   @include cwt-float(left, true);
///
@mixin cwt-float($float: null, $reset-list: false) {
	@include cwt-clearfix;
	@if $reset-list {
		@include reset-list;
	}
	@if isnotempty($float) {
		> * {
			float: $float !important;
			@content;
		}
	}
}

///
/// Defines an element's x and y overflow values
///
/// @param {String} $x - Element's x-overflow value (Optional)
/// @param {String} $y - Element's y-overflow value (Optional)
///
/// @example scss
///   @include overflow(auto, scroll);
///
@mixin overflow($x: null, $y: null) {

	@if isset($x) and $x == $y {
		overflow: $x;
	} @else {
		@if isset($x) {
			overflow-x: $x;
		}

		@if isset($y) {
			overflow-y: $y;
		}

	}

	@if $x == auto or $x == scroll or $y == auto or $y == scroll {
		-webkit-overflow-scrolling: touch;
	}
}

///
/// Sets `overflow: hidden` to the element<br>
/// NOTICE: This mixin is planned to be deprecated in next major release
///
/// @example scss
///   @include overflow-hidden;
///
@mixin overflow-hidden {
	overflow: hidden;
}

///
/// Sets `overflow: auto` and `-webkit-overflow-scrolling: touch;` to the element
///
/// @example scss
///   @include overflow-scroll;
///
@mixin overflow-scroll {
	overflow:                   auto;
	-webkit-overflow-scrolling: touch;
}

///
/// Sets the box sizing of an item and it's children
/// @link https://www.paulirish.com/2012/box-sizing-border-box-ftw/ Paul Irish: * { Box-sizing: Border-box } FTW
///
/// @param {String} $box-sizing [border-box] - Element's border-box value (Optional)
///
/// @example scss
///   @include cwt-box-sizing(content-box);
///
@mixin cwt-box-sizing($box-sizing: border-box) {
	$box-sizing: unquote($box-sizing);
	@if $box-sizing == true {
		$box-sizing: border-box;
	}
	&, &:before, &:after {
		box-sizing: $box-sizing;
	}
	*, *:before, *:after {
		box-sizing: inherit;
	}
}

///
/// Defines an element's width, height, line-height, and box-sizing
///
/// @param {Length} $width - Element's width
/// @param {Length} $height - Element's height
/// @param {Length} $line-height - Line height (Optional)
/// @param {String} $box-sizing - Defines box-sizing attribute. Use "true" for `border-box` (Optional)
///
/// @content Applies styles to element
///
/// @require {mixin} cwt-box-sizing
///
/// @example scss
///   @include cwt-size(100px, 200px, 1em, true);
///
@mixin cwt-size($width: null, $height: null, $line-height: null, $box-sizing: null) {
	@if $width != null {
		width: $width;
	}
	@if $height != null {
		height: $height;
	}
	@if $line-height != null {
		line-height: $line-height;
	}
	@if (isnotempty($box-sizing)) {
		@include cwt-box-sizing($box-sizing);
	}
	@content;
}

///
/// Creates a block element and calls cwt-size to define width, height, line-height, and box-sizing
/// @see {mixin} cwt-size
///
/// @param {Length} $width - Element's width
/// @param {Length} $height - Element's height
/// @param {Length} $line-height - Element's text Line height (Optional)
/// @param {String} $box-sizing - Defines box-sizing attribute. Use "true" for `border-box` (Optional)
///
/// @content Applies styles to element
///
/// @require {mixin} cwt-size
///
/// @example scss
///   @include cwt-block(100px, 200px, 1em, true);
///
@mixin cwt-block($width: null, $height: null, $line-height: null, $box-sizing: null) {
	display: block;
	@include cwt-size($width, $height, $line-height, $box-sizing) {
		@content;
	}
}

///
/// Calls the mixin cwt-size to define height and width as 100%. Also allows you to define box-sizing<br>
/// NOTICE: This mixin is planned to be deprecated in next major release
///
/// @param {String} $box-sizing - Defines box-sizing attribute. `content-box` is default. Use "True" for `border-box` (Optional)
///
/// @content Applies styles to element
///
/// @require {mixin} cwt-size
///
/// @example scss
///   @include cwt-full-size(true);
///
@mixin cwt-full-size($box-sizing: null) {
	@include cwt-size(100%, 100%, null, $box-sizing) {
		@content;
	}
}

///
/// Defines an element's z-index and it's top, right, bottom, and left values
///
/// @param {Number} $z-index - z-index (Optional)
/// @param {Length} $top - Top (Optional)
/// @param {Length} $right - Right (Optional)
/// @param {Length} $bottom - Bottom (Optional)
/// @param {Length} $left - Left (Optional)
///
/// @content Applies styles to element
///
/// @example scss
///   @include position(1, 100px, null, null, 200px);
///
@mixin position($z-index: null, $top: null, $right: null, $bottom: null, $left: null) {
	@if $z-index != null {
		z-index: $z-index;
	}
	@if $top != null {
		top: $top;
	}
	@if $bottom != null {
		bottom: $bottom;
	}
	@if $left != null {
		left: $left;
	}
	@if $right != null {
		right: $right;
	}
	@content;
}

///
/// Defines an element as absolute positioned and calls the mixin 'position' to set z-index and it's top, right, bottom, and left values
/// @see {mixin} position
///
/// @param {Number} $z-index - z-index (Optional)
/// @param {Length} $top - Top (Optional)
/// @param {Length} $right - Right (Optional)
/// @param {Length} $bottom - Bottom (Optional)
/// @param {Length} $left - Left (Optional)
///
/// @content Applies styles to element
///
/// @require {mixin} position
///
/// @example scss
///   @include absolute(1, 100px, null, null, 200px);
///
@mixin absolute($z-index: null, $top: null, $right: null, $bottom: null, $left: null) {
	position: absolute;
	@include position($z-index, $top, $right, $bottom, $left) {
		@content;
	}
}

///
/// Defines an element as relatively positioned and calls the mixin 'position' to set z-index and it's top, right, bottom, and left values
/// @see {mixin} position
///
/// @param {Number} $z-index - z-index (Optional)
/// @param {Length} $top - Top (Optional)
/// @param {Length} $right - Right (Optional)
/// @param {Length} $bottom - Bottom (Optional)
/// @param {Length} $left - Left (Optional)
///
/// @content Applies styles to element
///
/// @require {mixin} position
/// @example scss
///   @include relative(1, 100px, null, null, 200px);
///
@mixin relative($z-index: null, $top: null, $right: null, $bottom: null, $left: null) {
	-webkit-transform: translate3d(0, 0, 0); // safari bug
	position:          relative;
	@include position($z-index, $top, $right, $bottom, $left) {
		@content;
	}
}

///
/// Defines an element's position as fixed and calls the mixin 'position' to set z-index and it's top, right, bottom, and left values
/// @see {mixin} position
///
/// @param {Number} $z-index - z-index (Optional)
/// @param {Length} $top - Top (Optional)
/// @param {Length} $right - Right (Optional)
/// @param {Length} $bottom - Bottom (Optional)
/// @param {Length} $left - Left (Optional)
///
/// @content Applies styles to element
///
/// @require {mixin} position
///
/// @example scss
///   @include fixed(1, 100px, null, null, 200px);
///
@mixin fixed($z-index: null, $top: null, $right: null, $bottom: null, $left: null) {
	position: fixed;
	@include position($z-index, $top, $right, $bottom, $left) {
		@content;
	}
}

///
/// CSS Aspect Ratio Trick to force items to preserve an aspect ratio as the container size changes
/// @link https://css-tricks.com/aspect-ratio-boxes/ CSS-Tricks: Aspect Ratio Boxes
///
/// @param {Number} $w [1] - Horizontal unit
/// @param {Number} $h [1] - Vertical unit (Optional)
/// @param {Arglist} $absolute [[null, 0, 0, 0, 0]] - Absolute positioning of child within the container. I.E. if you want 10px use (null, 10px, 10px, 10px, 10px) (Optional)
///
/// @require {mixin} absolute
///
/// @content Applies styles to child element with ratio that will be preserved
///
/// @example scss
///   @include cwt-block-ratio(16, 9); // 1.778
///
@mixin cwt-block-ratio($w: 1, $h: 1, $absolute: (null, 0, 0, 0, 0)) {
	position: relative;
	&:before {
		content:     " ";
		display:     block;
		padding-top: number-format(percentage($h/$w), 2);
	}
	> * {
		@include absolute($absolute...);
		@content;
	}
}

///
/// A HTML/BODY tag reset. Sets font-size to 62.5% for use of REMs, sets box-sizing and resets the box model
///
/// @param {String} $box-sizing [border-box] - border-box value (Optional)
/// @param {String} $append-to-html-tag [""] - Custom css selector (Optional)
///
/// @require {mixin} reset-box-model
/// @require {mixin} cwt-box-sizing
/// @require {mixin} cwt-clearfix
///
/// @example scss
///   @include body-reset(border-box, ".cwt-custom-reset");
///
@mixin body-reset($box-sizing: border-box, $append-to-html-tag: "") {
	html#{$append-to-html-tag} {
		&, body {
			@include reset-box-model;
			min-height: 100%;
		}
	}

	html#{$append-to-html-tag} {
		font-size: 62.5%;
		@if isnotempty($box-sizing) {
			@include cwt-box-sizing($box-sizing);
		}
	}

	html#{$append-to-html-tag} body {
		@include cwt-clearfix;
	}
}

///
/// NOTICE: This alias is planned to be deprecated in next major release
/// @alias cwt-clearfix
///
@mixin body-clearfix($box-sizing: border-box, $append-to-html-tag: "") {
	@include body-reset($box-sizing, $append-to-html-tag);
}

///
/// Sets the size of the html and body tags to be 100% width and height for a defined media query
/// Styles are set to be ignored in print mode<br>
/// NOTICE: This mixin is planned to be deprecated in next major release
///
/// @param {String} $mq_query [$mq_mobile] - Defines media query size
/// @param {String} $append-to-html-tag [""] - Defines html element id (Optional)
///
/// @require {mixin} at_cwt_bp
/// @require {mixin} cwt-size
///
/// @example scss
///   @include body-reset-app-only("non-mobile", ".cwt-custom-app");
///
@mixin body-reset-app-only($mq_query: $mq_mobile, $append-to-html-tag: "") {
	html#{$append-to-html-tag} {
		@include at_cwt_bp($mq_query, true) {
			&, body {
				@include cwt-size(100%, 100%);
				overflow: hidden;
			}
		}
		@include at-print {
			&, body {
				@include cwt-size(auto, auto);
				overflow: auto;
			}
		}
	}
}

///
/// NOTICE: This alias and it's reference is planned to be deprecated in next major release
/// @alias body-reset-app
///
@mixin body-clearfix-app-only($mq_query: $mq_mobile, $append-to-html-tag: "") {
	@include body-reset-app-only($mq_query, $append-to-html-tag);
}

///
/// Combines the body-reset and body-reset-app-only
/// @see {mixin} body-reset
/// @see {mixin} body-reset-app-only
///
/// @param {String} $box-sizing [border-box] - Defines border-box value
/// @param {String} $mq_query [$mq_mobile] - Defines media query size
/// @param {String} $append-to-html-tag [""] - Defines html element id (Optional)
///
/// @require {mixin} body-reset
/// @require {mixin} body-reset-app-only
///
/// @example scss
///   @include body-reset-app(border-box, "non-mobile", ".cwt-custom-app");
///
@mixin body-reset-app($box-sizing: null, $mq_query: $mq_mobile, $append-to-html-tag: "") {
	@include body-reset($box-sizing, $append-to-html-tag);
	@include body-reset-app-only($mq_query, $append-to-html-tag);
}

///
/// NOTICE: This alias and it's reference is planned to be deprecated in next major release
/// @alias body-reset-app
///
@mixin body-clearfix-app($box-sizing: null, $mq_query: $mq_mobile, $append-to-html-tag: "") {
	@include body-reset-app($box-sizing, $mq_query, $append-to-html-tag);
}

///
/// Centers an element horizontally, vertically, or both
///
/// @param {Bool} $center-text [false] - Centers the text (Optional)
/// @param {String} $dir - Determines whether to center horizontally, vertically, or both (Optional)
///
/// @example scss
///   @include cwt-center-block(true, x);
///
@mixin cwt-center-block($center-text: false, $dir: null) {
	position: absolute;
	@if $center-text {
		text-align: center;
	}
	@if $dir == x {
		left:      50%;
		transform: translateX(-50%);
	} @else if $dir == y {
		top:       50%;
		transform: translateY(-50%);
	} @else {
		top:       50%;
		left:      50%;
		transform: translate(-50%, -50%);
	}
	@content;
}

///
/// Centers an element horizontally
/// @see {mixin} cwt-center-block
///
/// @param {Bool} $center-text [false] - Centers the text (Optional)
///
/// @content Applies styles to element
///
/// @require {mixin} cwt-center-block
///
/// @example scss
///   @include cwt-center-block-x(true);
///
@mixin cwt-center-block-x($center-text: false) {
	@include cwt-center-block($center-text, x) {
		@content;
	}
}

///
/// Centers an element vertically
/// @see {mixin} cwt-center-block
///
/// @param {Bool} $center-text [false] - Centers the text (Optional)
///
/// @content Applies styles to element
///
/// @require {mixin} cwt-center-block
///
/// @example scss
///   @include cwt-center-block-y(true);
///
@mixin cwt-center-block-y($center-text: false) {
	@include cwt-center-block($center-text, y) {
		@content;
	}
}

///
/// Aligns element to the top of its container
///
/// @param {String} $display [block] - Element's display value (Optional)
///
/// @content Applies styles to element
///
/// @require {mixin} absolute
///
/// @example scss
///   @include cwt-align-top(inline-block);
///
@mixin cwt-align-top($display: block, $z-index: null) {
	@if $display {
		display: $display;
	}
	@include absolute($z-index, 0, 0, null, 0);
	@content;
}

///
/// Aligns element to the bottom of its container
///
/// @param {String} $display [block] - Element's display value (Optional)
///
/// @content Applies styles to element
///
/// @require {mixin} absolute
///
/// @example scss
///   @include cwt-align-bottom(inline-block);
///
@mixin cwt-align-bottom($display: block, $z-index: null) {
	@if $display {
		display: $display;
	}
	@include absolute($z-index, null, 0, 0, 0);
	@content;
}

///
/// Uncollapses margins of an element
/// @link https://www.sitepoint.com/collapsing-margins/ SitePoint: Collapsing Margins
///
/// @example scss
///   @include cwt-uncollapse-margin;
///
@mixin cwt-uncollapse-margin {
	&:before, &:after {
		content:  "\00a0"; /* No-break space character */
		display:  block;
		overflow: hidden;
		height:   0;
	}
}

///
/// Collapses margins of an element
/// @link https://www.sitepoint.com/collapsing-margins/ SitePoint: Collapsing Margins
///
/// @example scss
///   @include cwt-collapse-margin;
///
@mixin cwt-collapse-margin {
	&:before, &:after {
		content:  initial;
		display:  initial;
		overflow: initial;
		height:   initial;
	}
}